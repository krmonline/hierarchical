<?php
namespace hierarchical;

use Phpml\Math\Distance\Euclidean;
class agglomerate {
  private $distance_limit;
  private $data;

  function __construct($data){
      $this->data = $data;
      //var_dump($data);
  }

  function pointToDistance(){
    //var_dump($this->data);
    $arr_key = array_keys($this->data);
    $arr_value = array_values($this->data);
    $k =0 ;
    for($i = 0;$i<count($arr_value);$i++){
      for($j=$i;$j<count($arr_value);$j++){
        $k++;
        if($i == $j){
           $value = 0;
        }else{
          //echo $k." ".$i,$j."\n";
          $distanceMetric = new Euclidean();
          $value = Euclidean::distance($arr_value[$i],$arr_value[$j]);
        }
        $arr_Distance[$i][$j] = $value;
        $arr_Distance[$j][$i] = $value;
      }
    }
    return array("name"=> $arr_key,"distance" => $arr_Distance);
  }

  function findMinAll($arr_distance){
    $i = 0;
    foreach($arr_distance as $ki => $vi){
      $j = 0;
      $i++;
      foreach($vi as $kj => $vj){
        $j++;
        if($j <= $i){
           continue;
        }
        if($kj == $ki){
          continue;
        }
        $distance = $vj;
        if(isset($minimum_distance)){
          if($minimum_distance > $distance){
              $minimum_distance = $distance;
              $p[0] = $ki;
              $p[1] = $kj;
              $p['dist'] = $distance;
          }
        }else{
          $minimum_distance = $distance;
          $p[0] = $ki;
          $p[1] = $kj;
          $p['dist'] = $distance;
        }
      }
    }
    //echo $p[0] ."\t".$p[1]."\t{$p['dist']}\n";
    if(!isset($p)){
      die("p not found");
    }
    return $p;
  }

  function remove($label,&$arr_label,&$arr_distance){
    $key = array_search($label,$arr_label);
    unset($arr_label[$key]);
    unset($arr_distance[$key]); //X axis
    foreach($arr_distance as &$item) { //Y axis
      unset($item[$key]);
      //echo "======== item[$key] has been unset =====\n";
    }
    unset($item);
    //echo "======================removed==================";
    //var_dump($arr_label);
    //var_dump($arr_distance);
  }

  function add($label0,$label1,&$arr_label,&$arr_distance){
    $label = $label0."|".$label1;
    $arr_label[] = $label;
    $k_new = array_search($label,$arr_label);
    foreach($arr_label as $k => $label_dest) {
        if($label_dest == $label){
           $minimum_distance = 0;
           $arr_distance[$k][$k] = 0;
        }else{
          $s1 = $label0;
          $s2 = $label1;
          $d = $label_dest;
          $minimum_distance = $this->findMin($s1,$s2,$d,$arr_label,$arr_distance);
          $arr_distance[$k_new][$k] = $minimum_distance;
          $arr_distance[$k][$k_new] = $minimum_distance;
        }
    }
    return $minimum_distance;
  }

  function findMin($s1,$s2,$d,$arr_label,$arr_distance){
    //echo("$s1\n");
    //var_dump($arr_label);
    //var_dump($arr_distance);
    $ks1 = array_search($s1,$arr_label);
    $ks2 = array_search($s2,$arr_label);
    $kd = array_search($d,$arr_label);
    if(!isset($arr_distance[$ks1][$kd])){
      die("error not found $ks1,$kd");
    }
    $ks1d = $arr_distance[$ks1][$kd];
    $ks2d = $arr_distance[$ks2][$kd];
    if($ks1d > $ks2d){
      $minimum_distance = $ks2d;
    }else{
      $minimum_distance = $ks1d;
    }
    return $minimum_distance;
  }


}

 ?>
